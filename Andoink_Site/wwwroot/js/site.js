﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Instantiate the bootstrap carousel
$('.multi-item-carousel').carousel({
    interval: false
});

// For ever slide in carousel, copy the next slide's item into the slide
$('.multi-item-carousel .item').each(function () {
    var next = $(this).next();
    if (!next.length) {
        next = $(this).siblings(':first');
    }
    next.children(':first-child').clone().appendTo($(this));

    if (next.next().length > 0) {
        next.next().children(':first-child').clone().appendTo($(this));
    } else {
        $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
    }
});


// Toggle between showing and hiding the sidebar when clicking the menu icon
var mySidebar = document.getElementById("mySidebar");

// Show navigation sidebar
function w3_open() {
    if (mySidebar.style.display === 'block') {
        mySidebar.style.display = 'none';
    } else {
        mySidebar.style.display = 'block';
    }
}

// Close the navigation sidebar
function w3_close() {
    mySidebar.style.display = "none";
}



function overlay_on() {
    document.getElementById("overlay").style.display = "block";
    //document.getElementById("overlay").style.src = this.src;
}

//function overlay_on(img) {
//    document.getElementById("overlay").style.display = "block";
//    document.getElementById("overlay").style.src = img;
//}

function overlay_off() {
    document.getElementById("overlay").style.display = "none";
}



// Get the modal and image
var modal = document.getElementById('myModal');
var modalImg = document.getElementById("img01");

function modal_on(img) {
    img = "/images/hbj/" + img;
    modalImg.src = img;        
    modal.style.display = "block";
}

// Show the modal on button click
//btn.onclick = function () {
//    modal.style.display = "block";
//    /*modalImg.src = this.src;*/
//}

// Close modal image when clicked
modalImg.onclick = function () {
    modal.style.display = "none";
}