﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Andoink_Site.Pages
{
    public class IndexModel : PageModel
    {
        // Carousel text
        public string CarouselMeMessage { get; set; }
        public string CarouselDevMessage { get; set; }
        public string CarouselDevButtonMessage { get; set; }
        public string CarouselPrintMessage { get; set; }
        public string CarouselPrintButtonMessage { get; set; }
        public string CarouselFilmMessage { get; set; }
        public string CarouselFilmButtonMessage { get; set; }

        public string AboutMe { get; set; }
        public string Andoink = "Andoink";

        public void OnGet()
        {
            CarouselMeMessage = "Welcome to Andoink.com!" + Environment.NewLine + "All about me, my skills, my hobbies, and my life";

            CarouselDevMessage = "Working to become an indie game developer - Doink Games." + Environment.NewLine + "Check out my work!";
            CarouselDevButtonMessage = "Oh neat";

            CarouselPrintMessage = "Take a look at my 3D-printing projects!" + Environment.NewLine + "Failure is definitely an option...";
            CarouselPrintButtonMessage = "How nifty";

            CarouselFilmMessage = "If for some reason you care what I think of various films..." + Environment.NewLine + "Browse through my reviews!";
            CarouselFilmButtonMessage = "Eh ok, but I'll probably disagree";

            AboutMe = "I'm Andy, but I tend to use the handle \"" + Andoink + "\" online and for my projects. "
                + "Thanks for visiting my very simple site! There's really not much here, but I'll build it out slowly. "
                + "It may not look like much, but it's custom made from scratch using .NET Core 2.1 Razor Pages. "
                + "So this is a thing that I made. Yup. Incredible, I know. Calm down. Hold yourself together. "
                + "It's exciting but there's no need for all that. There's not even anything to do here yet... "
                + Environment.NewLine + Environment.NewLine
                + "In the meantime, take a look at my development work for Doink Games and be amazed! "
                + "Go be awestruck at my 3D-printing creations (or have a good laugh at the mishaps)! "
                + "Head on over to my film reviews and whine about my ratings! "
                + Environment.NewLine + Environment.NewLine
                + "Or go to another website with actual content. "
                + Environment.NewLine + Environment.NewLine
                + "Wait, how did you find me anyway? GUARDS!";
        }
    }
}
