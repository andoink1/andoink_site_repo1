﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Andoink_Site.Pages
{
    public class PrivacyModel : PageModel
    {
        public string Message { get; set; }

        public void OnGet()
        {
            Message = "You're here, accept it. Is this page supposed to be informative?"
                + Environment.NewLine
                + "It's safe, but I'll put some GDPR mumbo jumbo or something in here soon.";
        }
    }
}